plugins {
    java
    groovy
}

group = "io.higson"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType(JavaCompile::class) {
    options.encoding = "UTF-8"
}

// see gradle/libs.versions.toml
dependencies {
    implementation(libs.groovy.core)
    implementation(libs.groovy.json)
    implementation(libs.geb.spock)
    implementation(libs.webdrivermanager)
    implementation(libs.httpclient5)

    testRuntimeOnly(libs.jupiter.engine)
    testImplementation(libs.slf4j.api)
    testImplementation(libs.slf4j.simple)
    testImplementation(libs.jupiter.api)
    testImplementation(libs.selenide)
    testImplementation(libs.spock.reports) {
        isTransitive = false
    }
}

tasks {
    test { selenideTest(browser = "firefox") }
    selenideTest(browser = "chrome")
    selenideTest(browser = "firefox")
}

fun TaskContainerScope.selenideTest(browser: String) {
    register<Test>(browser + "Test") {
        useJUnitPlatform()
        // see com.codeborne.selenide.SelenideConfig
        systemProperties = mapOf(
            "selenide.browser" to browser,
            "selenide.baseUrl" to "https://workshop-automation.vercel.app",
            "selenide.timeout" to "2000",
            "geb.build.reportsDir" to "build/spock-reports"
        )
    }
}