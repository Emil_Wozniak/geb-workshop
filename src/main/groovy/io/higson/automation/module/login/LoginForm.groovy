//file:noinspection unused
package io.higson.automation.module.login

import geb.Module

class LoginForm extends Module {
    static def content = {
        emailField { $('form').email() }
        passwordField { $('form').password() }
    }

    void login(String username, String password) {
        emailField << username
        passwordField << password
    }
}
