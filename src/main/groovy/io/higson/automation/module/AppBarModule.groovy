//file:noinspection unused
package io.higson.automation.module

import geb.Module

class AppBarModule extends Module {
    static content = {
        appBar { $("div.app-bar") }
        appTitle { $("a#page-title") }
        postsIcon(required: false) { $("a[href = '/posts']") }
        aboutIcon { $("a[href = '/about']") }
        loginIcon { $("a[href = '/login']") }
    }

    boolean exists() {
        def all = appBar && appTitle.text() == "GEB Workshop" && aboutIcon && loginIcon
        all
    }
}
