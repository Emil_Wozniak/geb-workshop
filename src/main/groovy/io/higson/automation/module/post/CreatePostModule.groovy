//file:noinspection unused
package io.higson.automation.module.post

import geb.Module
import org.openqa.selenium.Keys

class CreatePostModule extends Module {
    static content = {
        titleInput { $('input#title') }
        tagsInput {
            $('div.input-chip-wrapper')
                    .$('form')
                    .$("#input-chip-field")
        }
        contentInput {
            $("#tiptap")
        }
        postButton { $("button[type ='submit']") }
    }

    boolean createPost(String title, String content, List<String> tags) {
        waitFor {
            titleInput && tagsInput && contentInput
        }

        titleInput.click()
        titleInput << tagsInput
        tagsInput.click()
        tags.each { tag ->
            tagsInput << tag
            tagsInput << Keys.ENTER
        }
        contentInput.click()
        contentInput << content
        postButton.click()
        return true
    }
}
