package io.higson.automation.module.post

import geb.Module

class PostModule extends Module {
    static content = {
        postTags { $("a#post-tags") }
        postTitle { $("a#post-title") }
        postContent { $("p#post-content") }
    }
}
