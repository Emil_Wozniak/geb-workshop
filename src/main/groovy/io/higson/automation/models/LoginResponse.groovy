package io.higson.automation.models

import groovy.transform.Canonical

@Canonical
class LoginResponse {
    String token
}

@Canonical
class LoginRequest {
    String email
    String password

    static of(String email, String password) {
        new LoginRequest(email, password)
    }
}
