//file:noinspection unused
package io.higson.automation.page

import io.higson.automation.module.AppBarModule

class HomePage extends BasePage {
    static url = "/" // TODO
    static atCheckWaiting = true
    static at = {
        true
        // TODO
        // appbar is displayed
        // title is displayed
    }

    static content = {
        appbar { module(AppBarModule) }
        // TODO
        // appbar
        // title = E2E with Geb - workshop
        // image with alt geb icon
        // Spock up and running link
        // card element
        // card-header element
    }
}
