//file:noinspection unused
package io.higson.automation.page

import geb.Page
import io.higson.automation.module.AppBarModule
import io.higson.automation.module.post.PostModule

class PostsPage extends Page {
    static url = "/posts"
    static atCheckWaiting = true
    static at = {
        true
        appbar
        // TODO title is displayed
    }

    static content = {
        appbar { module(AppBarModule) }
        // TODO title h2
        // TODO create post button
        postsSection { $("section#posts-section") }
        postArticles(required: false) {
            $('article#post').collect { it.module(PostModule) }
        }
    }

}
