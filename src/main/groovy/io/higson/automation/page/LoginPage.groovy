//file:noinspection unused
package io.higson.automation.page

import io.higson.automation.module.AppBarModule
import io.higson.automation.module.login.LoginForm

class LoginPage extends BasePage {
    static url = "/login"
    static at = {
        loginForm
        appBar.exists()
    }

    static atCheckWaiting = true

    static content = {
        appBar { module(AppBarModule) }
        loginForm { module(LoginForm) }
        loginButton { $("button#login-button") }
        errorSection(required: false) { $("div[role='alertdialog']") }
    }

    void login(String username, String password) {
        waitFor { loginForm }
        loginForm.login username, password
        loginButton.click()
    }
}
