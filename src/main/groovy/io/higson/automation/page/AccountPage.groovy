//file:noinspection unused
package io.higson.automation.page

import geb.Page

class AccountPage extends Page {
    static url = "/user/account"
    static atCheckWaiting = true
    static at = {
        titleField
        emailField
        passwordField
        logoutBtn
    }

    static content = {
        titleField { $("#title") }
        fullNameField { $("#user-name") }
        emailField { $("#user-email") }
        passwordField { $("#user-password-hidden") }
        logoutBtn { $("button#logout-btn") }
    }
}
