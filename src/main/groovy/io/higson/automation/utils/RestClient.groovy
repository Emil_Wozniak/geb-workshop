package io.higson.automation.utils

import groovy.json.JsonBuilder
import groovy.json.JsonSlurperClassic
import groovy.transform.Canonical
import io.higson.automation.models.LoginRequest
import io.higson.automation.models.LoginResponse

class RestClient {
    private def baseUrl
    private def proxy = Proxy.NO_PROXY
    private String token
    private static parser = new JsonSlurperClassic()

    String login(String email, String password) {
        def response = post "/api/login",
                LoginResponse,
                LoginRequest.of(email, password)
        if (response.status == 200) {
            token = response.payload.token
            return response.payload.token
        }
        else null
    }

    def logout() {
        token = null
    }

    RestClient(baseUrl) {
        this.baseUrl = baseUrl
    }

    static def encodePath(path) {
        path.replaceAll(" ", "%20")
    }

    <R> Response<R> get(String path, Class<R> type) {
        request Method.GET, path, type
    }

    <R> Response<R> post(String path, Class<R> type, content = null) {
        request Method.POST, path, type, content
    }

    <R> Response<R> put(String path, Class<R> type, content = null) {
        request Method.PUT, path, type, content
    }

    def delete(String path) {
        request Method.DELETE, path, Void
    }

    private <T> Response<T> request(Method method, String path, Class<T> type, outContent = null) {
        println("Open connection: ${method.name()} $baseUrl$path")
        def http = createConnection("$baseUrl$path").tap { it ->
            requestMethod = method.name()
            doInput = true
            doOutput = (outContent != null)
            if (token) {
                setRequestProperty "Authorization", "Bearer ${token.bytes.encodeBase64().toString()}"
            }
        }
        http.connect()
        try {
            def json = new JsonBuilder(outContent).toPrettyString()
            println "Request payload: $json"
            outContent?.with { http.outputStream.withWriter { it.write json } }
            def status = http.responseCode
            def payload = (status >= 400
                    ? http.errorStream
                    : http.inputStream
            )?.text

            new Response<T>(status, parse(payload, type))
        } finally {
            http.disconnect()
        }
    }

    private HttpURLConnection createConnection(String url) {
        new URL(url).openConnection(proxy) as HttpURLConnection
    }

    private static <T> T parse(String text, Class<T> type) {
        println(text)
        def result = parser.parseText(text)
        return result as T
    }

    @Canonical
    class Response<T> {
        int status
        T payload
    }

    private enum Method {
        GET, POST, PUT, DELETE
    }
}
