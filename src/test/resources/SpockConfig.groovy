this.getClass().getClassLoader().getParent().parseClass '''
  package groovy.util.slurpersupport
  class GPathResult{}
'''

runner {
    filterStackTrace false
}

spockReports {
    set 'com.athaydes.spockframework.report.showCodeBlocks': false
}