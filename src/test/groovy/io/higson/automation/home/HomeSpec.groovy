package io.higson.automation.home

import io.higson.automation.BaseGebSpec
import io.higson.automation.page.HomePage
import spock.lang.Narrative
import spock.lang.Title

@Title("Home page specification")
// TODO add narrative
@Narrative("""

""")
class HomeSpec extends BaseGebSpec {

    def "authenticated user should see posts icon"() {
        given:
        page = to HomePage
        authenticate "admin@geb.pl", "admin"

        expect:
        page.appbar.postsIcon
    }

    def "Not logged in user is in"() {
        given: "User page"
        page = to HomePage // 1. TODO implement home page

        expect: "Page has a title"
        // TODO page contains title with text E2E with Geb - workshop

    }
}
