package io.higson.automation.login

import io.higson.automation.BaseGebSpec
import io.higson.automation.page.AccountPage
import io.higson.automation.page.LoginPage
import spock.lang.Narrative
import spock.lang.Title

@Title("Login page specification")
@Narrative("""
- user should login when correct credentials were provided and submit
- user which provides incorrect credentials and submit should stay on login page
- user which provides incorrect credentials and submit should see error message in error section
""")
class LoginSpec extends BaseGebSpec {
    def "when user login as '#username' with password '#password' ends on #locationName"() {
        given:
        page = to LoginPage

        when: "user try to login"
        page.login username, password

        then: "user is at #locationName"
        at location

        where:
        username           | password           || location
        VALID_EMAIL        | VALID_PASSWORD     || AccountPage
        "invalid@username" | VALID_PASSWORD     || LoginPage
        VALID_EMAIL        | "invalid password" || LoginPage
        locationName = location.simpleName
                .replace('Page', '')
                .toLowerCase()
    }

    def "when user login with incorrect credentials error section is presented"() {
        given: "login page"
        page = to LoginPage

        when: "user try to login"
        page.login "invalid@username", "invalid password"

        then: "user stays on login page"
        at LoginPage

        and: "error section is presented"
        waitFor { page.errorSection }
        with(page.errorSection) {
            displayed
            text().contains "Invalid login or password"
        }
    }
}