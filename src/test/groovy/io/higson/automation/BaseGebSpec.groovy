package io.higson.automation

import com.codeborne.selenide.Configuration
import geb.Page
import geb.spock.GebReportingSpec
import io.higson.automation.utils.RestClient
import org.openqa.selenium.Cookie

class BaseGebSpec extends GebReportingSpec {
    protected static VALID_EMAIL = "admin@geb.pl"
    protected static VALID_PASSWORD = 'admin'
    protected static http = new RestClient(Configuration.baseUrl)

    def setupSpec() {
        def timeout = Configuration.timeout / 1000
        baseUrl = Configuration.baseUrl
        config.setAtCheckWaiting(timeout)
        config.setBaseNavigatorWaiting(timeout)
        Configuration.browserSize = "1280x800"
    }

    def authenticate(String email = "admin@geb.pl", String password = "admin") {
        def token = http.login(email, password)
        Cookie tokenCookie = new Cookie("token", token)
        browser.driver.manage().addCookie(tokenCookie)

        refreshWaitFor {
            page.appbar.postsIcon
        }
    }

    protected <T extends Page> T to(Class<T> pageType) {
        waitFor {
            if (page.class == pageType) {
                page
            } else {
                Map params = [:]
                Object[] args = []
                to(params, pageType, args)
            }
        } as T
    }
}
