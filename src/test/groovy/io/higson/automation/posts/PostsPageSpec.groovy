package io.higson.automation.posts

import io.higson.automation.BaseGebSpec
import io.higson.automation.page.PostsPage
import spock.lang.Narrative
import spock.lang.Stepwise
import spock.lang.Title

@Title("Posts page specification")
// TODO add narrative
@Narrative("""

""")
@Stepwise
class PostsPageSpec extends BaseGebSpec {

    def "user is able to create post"() {
        given: "posts page"
        page = to PostsPage
        // TODO authenticate

        when: "user clicks create post"
        // TODO click create post button

        then:
        // TODO user is at create post page
        true

        and: "user creates new post"
    }

    def "user is able to see created post"() {
        given: "user is at posts page"
        page = to PostsPage
        // TODO authenticate

        expect: "post articles are not empty"
        // TODO
        true

        and: "any post has new post title, content and tags"
        // TODO
    }

    private String NEW_POST_TITLE = ""
    private String NEW_POST_CONTENT = ""
    private List<String> NEW_POST_TAGS = []
}
